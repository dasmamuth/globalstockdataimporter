﻿using System;
using Microsoft.AspNetCore.Mvc;

namespace GlobalStockDataImporter.Controllers
{
    [ApiController]
    [Route("[controller]")]

    public class HealthCheck : ControllerBase
    {
        [HttpGet]
        public bool Get() { return true; }
    }
}
