﻿using System;
using System.Linq;
using System.Threading.Tasks;
using GlobalStockDataImporter.Interfaces;
using GlobalStockDataImporter.Model;
using MongoDB.Entities;
using MongoDB.Driver.Linq;
using System.Collections.Generic;

namespace GlobalStockDataImporter.Data
{
    public class ETailorRepository : IETailorRepository
    {
        //Instanciate the Db connection
        public ETailorRepository(IETailorsDatabaseSettings settings)
        {
            DB.InitAsync("UAT_Inventory", settings.ConnectionString, 27017);
        }
        //

        //Get ETailor by KUNNR
        public async Task<ETailors> GetEtailorName(string ETailorNumber)
        {
            var EtailorName = await DB.Queryable<ETailors>()
                                                .Where(a => a.Number.Contains(ETailorNumber))
                                                .SingleOrDefaultAsync();
            return (EtailorName);
        }
        //

        //Get all ETailors
        public async Task<List<ETailors>> GetAvailableETailors()
        {
            return await DB.Find<ETailors>().ExecuteAsync();
        }
        //
    }
}
