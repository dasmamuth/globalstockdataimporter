﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GlobalStockDataImporter.Interfaces;
using GlobalStockDataImporter.Messaging.Sender.v1;
using GlobalStockDataImporter.Model;
using Microsoft.Extensions.Options;
using static GlobalStockDataImporter.Model.EventModel;

namespace GlobalStockDataImporter.Data
{
    public class MessagingRepository:IMessageRepository
    {
        private readonly IProductImportSender _productImportSender = null;
        private readonly IEventHandlerSender _eventHandlerSender = null;
        private readonly IETailorRepository _eTailorRepository = null;
        Events Event = new Events();

        public MessagingRepository(IETailorRepository eTailorRepository, IOptions<RabbitMqConfiguration> rabbitMqOptions)
        {
            _eTailorRepository = eTailorRepository;
            _productImportSender = new ProductImportSender(rabbitMqOptions);
            _eventHandlerSender = new EventHandlerSender(rabbitMqOptions);
        }

        //Convert the Origin SAP Model into a extended one with ETailorList(string) included
        public SapDataModelwithETailorList ConvertProduct(SapDataModel ProductModel)
        {
            SapDataModelwithETailorList ConvertedProduct = new SapDataModelwithETailorList();
            List<string> ETailorNames = new List<string>();

            ConvertedProduct.MATNR = ProductModel.MATNR;
            ConvertedProduct.GRID = ProductModel.GRID;
            ConvertedProduct.QUANTITY = ProductModel.QUANTITY;
            ConvertedProduct.TIMESTAMP = ProductModel.TIMESTAMP;
            ConvertedProduct.KUNNR = ProductModel.KUNNR;
            ConvertedProduct.NAME1 = ProductModel.NAME1;
            ConvertedProduct.COLOR = ProductModel.COLOR;
            ConvertedProduct.CUP = ProductModel.CUP;
            ConvertedProduct.ZSIZE = ProductModel.ZSIZE;
            ConvertedProduct.MATERIAL_PRICE_ZONE = ProductModel.MATERIAL_PRICE_ZONE;
            ConvertedProduct.ETailorList = ETailorNames;
            return (ConvertedProduct);
        }
        //

        //First Step of Data enriching with calculation of common Stock and List of SalesChannels based on the SAP File
        public List<SapDataModelwithETailorList> PrepareStockDataFromSAPImport(List<SapDataModel> Products)
        {
            List<SapDataModelwithETailorList> products = new List<SapDataModelwithETailorList>();
            Dictionary<string, SapDataModelwithETailorList> Dict = new Dictionary<string, SapDataModelwithETailorList>();
            SapDataModelwithETailorList TempValueHolder = new SapDataModelwithETailorList();
            string EName = string.Empty; ; string result = string.Empty; ;

            List<string> ETailorList = new List<string>();
            Dictionary<string, string> ETailorNameDict = new Dictionary<string, string>();

            //Getting the List of ETailors with KUNNR and Name from the DB
            foreach (var e in _eTailorRepository.GetAvailableETailors().Result)
            {
                foreach (var el in e.Number)
                {
                    ETailorNameDict.Add(key: el, value: e.SalesChannelName);
                }
            }
            //

            foreach (var p in Products)
            {
                var ConvertedProduct = ConvertProduct(p);

                //Combining the Materialnumber with Grid to get a EAN number; search for that EAN in a selfprepared list; if available, update the stock, if not, add it to the list
                //Check the Customernumber against the list of supported customers in the Db; if a match is there, add it; if not don't do smth.
                string sku = ConvertedProduct.MATNR + ConvertedProduct.GRID;

                if (ETailorNameDict.TryGetValue(p.KUNNR, out result)) { EName = result; }

                if (Dict.TryGetValue(sku, out TempValueHolder))
                {
                    TempValueHolder.QUANTITY = TempValueHolder.QUANTITY + p.QUANTITY;
                    if (EName != "") { ETailorList.Add(EName); } else { ETailorList.Add(p.KUNNR); }
                    TempValueHolder.ETailorList.Clear();
                    TempValueHolder.ETailorList.AddRange(ETailorList);
                }
                else
                {
                    ETailorList.Clear();
                    if (EName != "") { ETailorList.Add(EName); } else { ETailorList.Add(p.KUNNR); }
                    ConvertedProduct.ETailorList.AddRange(ETailorList); 

                    Dict.Add(key:sku, value: ConvertedProduct);
                }
            }

            //Add all relevant products to the final Dictionary 
            foreach (var prod in Dict.Values) { products.Add(prod); }
            return (products);
        }
        //

        //Standard Import process for the JPN SQL Request
        public Task<bool> ProcessSQLProductInformations(List<JPNSQLDataModel.Entry1> Productlist)
        {
            Event.Date = DateTime.Now; Event.Process = "Message SQL Update Process"; Event.Service = "GlobalStockDataImporter"; Event.Event = ""; _eventHandlerSender.PrepareProductforSending(Event);
            List<ProductModel.Product> ListofProductModels = new List<ProductModel.Product>();
            ProductModel.Product test = new ProductModel.Product();
            Dictionary<string, List<ProductModel.Product>> MessageImportDict = new Dictionary<string, List<ProductModel.Product>>();
            try
            {
                //Matching the SQL Response into the Product Datamodel 
                foreach (var product in Productlist)
                {
                    ProductModel.Product productmodel = new ProductModel.Product();
                    ProductModel.Sku productsku = new ProductModel.Sku();
                    ProductModel.ETailorspec skueTailorspec = new ProductModel.ETailorspec();
                    ProductModel.Jpn jpn = new ProductModel.Jpn();
                    ProductModel.Rakuten rakuten = new ProductModel.Rakuten();
                    ProductModel.Yahoo yahoo = new ProductModel.Yahoo();
                    List<string> ItemIDs = new List<string>();
                    List<string> Channels = new List<string>();

                    rakuten.Color = product.C_COL;
                    rakuten.Cup = product.C_CUPSIZE;
                    if (!ItemIDs.Contains(product.URL)) ItemIDs.Add(product.URL);
                    rakuten.ItemID = ItemIDs;

                    yahoo.Color = "";
                    yahoo.Cup = "";
                    ItemIDs.Add("");
                    yahoo.ItemID = ItemIDs;

                    jpn.Rakuten = rakuten;
                    jpn.Yahoo = yahoo;

                    skueTailorspec.jpn = jpn;

                    productsku.etailorspec = skueTailorspec;
                    productsku.color = product.COL;
                    productsku.cup = product.CUP;
                    productsku.material = product.Material;
                    productsku.size = product.SIZE;
                    if (product.CUP==null)
                    {
                        productsku.stockkeepingunit = product.Material + product.COL + product.CUP + product.SIZE; ;
                    }
                    else
                    {
                        productsku.stockkeepingunit = product.Material + product.COL + product.CUP + product.SIZE; ;
                    }
                    productmodel.timestamp = DateTime.Now;
                    productmodel.sku = productsku;

                    Channels.Add("Rakuten");
                    productmodel.channel = Channels;

                    int index = ListofProductModels.FindIndex(a => a.sku.stockkeepingunit == productmodel.sku.stockkeepingunit);

                    if (index != -1)
                    {
                        var p = ListofProductModels[index];
                        p.sku.etailorspec.jpn.Rakuten.ItemID.Add(productmodel.sku.etailorspec.jpn.Rakuten.ItemID[0]);

                    }
                    else
                    {
                        ListofProductModels.Add(productmodel);
                    }

                }
                foreach (var j in splitList(ListofProductModels, 100000))
                {
                    MessageImportDict.Add(key: "SQL", value: j);
                    _productImportSender.PrepareProductforSendingDict(MessageImportDict);
                    MessageImportDict.Clear();
                }
            }
            catch (Exception ex)
            {
                Event.Date = DateTime.Now;
                Event.Process = "Error SQL Update Process";
                Event.Service = "GlobalStockDataImporter";
                Event.Event = ex.Message;
                _eventHandlerSender.PrepareProductforSending(Event);
            }
            return Task.FromResult(true);
        }
        //

        //Standard Import for the SAP File 
        public Task<bool> ProcessSAPProductInformations(List<SapDataModel> Productlist)
        {
            Event.Date = DateTime.Now; Event.Process = "Message SAP Update Process"; Event.Service = "GlobalStockDataImporter"; Event.Event = ""; _eventHandlerSender.PrepareProductforSending(Event);
            List<ProductModel.Product> ListofProductModels = new List<ProductModel.Product>();
            Dictionary<string, List<ProductModel.Product>> MessageImportDict = new Dictionary<string, List<ProductModel.Product>>();
            List<string> ListofImportedSKUs = new List<string>();
            Dictionary<string, List<string>> MessageImportDictForAlreadyImportedProducts = new Dictionary<string, List<string>>();
            int i = 0;

            var AccumulatedProducts = splitList(PrepareStockDataFromSAPImport(Productlist),100000);

            try
            {
                // //Matching the SAP File Structure Informations into the Product Datamodel 
                foreach (var SplittedProductLists in AccumulatedProducts)
                { 
                        foreach (var product in SplittedProductLists)
                        { 
                            ProductModel.Product productmodel = new ProductModel.Product();
                            ProductModel.Sku productsku = new ProductModel.Sku();
                            ProductModel.ETailorspec skueTailorspec = new ProductModel.ETailorspec();
                            ProductModel.Jpn jpn = new ProductModel.Jpn();
                            ProductModel.Rakuten rakuten = new ProductModel.Rakuten();
                            ProductModel.Yahoo yahoo = new ProductModel.Yahoo();
                            List<string> RakutenItemIDs = new List<string>();
                            List<string> YahooItemIDs = new List<string>();
                            List<string> Channels = new List<string>();

                            rakuten.Color = "";
                            rakuten.Cup = "";
                            RakutenItemIDs.Add("");
                            rakuten.ItemID = RakutenItemIDs;

                            yahoo.Color = "";
                            yahoo.Cup = "";

                            if (product.MATERIAL_PRICE_ZONE != "")
                            {
                                YahooItemIDs.Add(product.MATERIAL_PRICE_ZONE);
                                yahoo.ItemID = YahooItemIDs;
                            }
                            else
                            {
                                YahooItemIDs.Add("");
                                yahoo.ItemID = YahooItemIDs;
                            }
                        
                            jpn.Rakuten = rakuten;
                            jpn.Yahoo = yahoo;
                            skueTailorspec.jpn = jpn;
                            productsku.etailorspec = skueTailorspec;

                            productsku.color = product.GRID.Substring(0, 4);
                            productsku.cup = product.GRID.Substring(4, 1);
                            productsku.material = product.MATNR;
                            productsku.size = product.GRID.Substring(5);
                            productsku.stockkeepingunit = product.MATNR + product.GRID;

                            ListofImportedSKUs.Add(productsku.stockkeepingunit);

                            productmodel.stocklevel = Convert.ToInt32(product.QUANTITY);
                            try { productmodel.timestamp = DateTime.ParseExact(product.TIMESTAMP, "yyyyMMddHHmmss", System.Globalization.CultureInfo.InvariantCulture); }
                            catch { productmodel.timestamp = DateTime.Now; }

                            productmodel.sku = productsku;
                            productmodel.channel = product.ETailorList;
                            ListofProductModels.Add(productmodel);
                    }
                    //Adoption for List of Products to Send
                        MessageImportDict.Add(key: "SAP", value: ListofProductModels);
                        _productImportSender.PrepareProductforSendingDict(MessageImportDict);
                    //**
                    ListofProductModels.Clear(); MessageImportDict.Clear(); 
                }

                MessageImportDictForAlreadyImportedProducts.Add(key: "SAPCheckFor0Products", value: ListofImportedSKUs);
                _productImportSender.PrepareSAPCheckforSendingDict(MessageImportDictForAlreadyImportedProducts);
                ListofImportedSKUs.Clear(); MessageImportDictForAlreadyImportedProducts.Clear();
            }
            catch (Exception ex)
            {
                Event.Date = DateTime.Now;
                Event.Process = "Error SAP Update Process";
                Event.Service = "GlobalStockDataImporter";
                Event.Event = ex.Message;
                _eventHandlerSender.PrepareProductforSending(Event);
            }
            return Task.FromResult(true);
        }
        //

        //Chunking the Lists into parts
        public static List<List<T>> splitList<T>(List<T> locations, int nSize)
        {
            var list = new List<List<T>>();

            for (int i = 0; i < locations.Count; i += nSize)
            {
                list.Add(locations.GetRange(i, Math.Min(nSize, locations.Count - i)));
            }
            return list;
        }

    }
}
