﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using GlobalStockDataImporter.Interfaces;
using GlobalStockDataImporter.Model;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using RabbitMQ.Client;

namespace GlobalStockDataImporter.Messaging.Sender.v1
{
    public class ProductImportSender:IProductImportSender
    {
        private readonly string _hostname;
        private readonly string _queueName;
        private readonly string _queueName2;
        private readonly string _username;
        private readonly string _password;
        private ConnectionFactory _factory;
        private IModel _channel;
        private readonly IConnection _connection;

        public ProductImportSender(IOptions<RabbitMqConfiguration> rabbitMqOptions)
        {
            _hostname = rabbitMqOptions.Value.Hostname;
            _queueName = rabbitMqOptions.Value.OutProductQueue;
            _queueName2 = rabbitMqOptions.Value.OutProductQueueSAPSpecial;
            _username = rabbitMqOptions.Value.UserName;
            _password = rabbitMqOptions.Value.Password;
            _factory = new ConnectionFactory() { HostName = _hostname, UserName = _username, Password = _password };
            _connection = _factory.CreateConnection();
            _channel = _connection.CreateModel();
            _channel.QueueDeclare(queue: _queueName, durable: true, exclusive: false, autoDelete: false, arguments: null);
        }

        //Sending Message by Product for Import and Creation of Products
        public async Task SendMessage(byte[] body)
        {
            try
            {
                _channel.BasicPublish(exchange: "", routingKey: _queueName, basicProperties: null, body: body);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //Sending Message with 0 Products missed by the SAP Export for being checked against the Db
        public async Task SendMessageSAP(byte[] body)
        {
            try
            {
                _channel.BasicPublish(exchange: "", routingKey: _queueName2, basicProperties: null, body: body);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //debrecated; not need anymore
        public async Task PrepareProductforSending(List<ProductModel.Product> product)
        {
            try
            {
                var json = JsonConvert.SerializeObject(product);
                var body = Encoding.UTF8.GetBytes(json);

                await SendMessage(body);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task PrepareProductforSendingDict(Dictionary<string,List<ProductModel.Product>> product)
        {
            try
            {
                var json = JsonConvert.SerializeObject(product);
                var body = Encoding.UTF8.GetBytes(json);

                await SendMessage(body);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task PrepareSAPCheckforSendingDict(Dictionary<string, List<string>> product)
        {
            try
            {
                var json = JsonConvert.SerializeObject(product);
                var body = Encoding.UTF8.GetBytes(json);

                await SendMessageSAP(body);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}
