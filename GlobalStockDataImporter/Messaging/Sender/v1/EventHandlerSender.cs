﻿using System;
using System.Text;
using System.Threading.Tasks;
using GlobalStockDataImporter.Interfaces;
using GlobalStockDataImporter.Model;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using RabbitMQ.Client;
using static GlobalStockDataImporter.Model.EventModel;
using static GlobalStockDataImporter.Model.Settings;

namespace GlobalStockDataImporter.Messaging.Sender.v1
{
    public class EventHandlerSender:IEventHandlerSender
    {
        private readonly string _hostname;
        private readonly string _queueName;
        private readonly string _username;
        private readonly string _password;
        private ConnectionFactory _factory;
        private IModel _channel;
        private readonly IConnection _connection;

        public EventHandlerSender(IOptions<RabbitMqConfiguration> rabbitMqOptions)
        {
            _hostname = rabbitMqOptions.Value.Hostname;
            _queueName = rabbitMqOptions.Value.EventHandler;
            _username = rabbitMqOptions.Value.UserName;
            _password = rabbitMqOptions.Value.Password;
            _factory = new ConnectionFactory() { HostName = _hostname, UserName = _username, Password = _password };
            _connection = _factory.CreateConnection();
            _channel = _connection.CreateModel();
            _channel.QueueDeclare(queue: _queueName, durable: true, exclusive: false, autoDelete: false, arguments: null);
        }

        //Sending Message by Product for Import and Creation of Products
        public async Task SendMessage(byte[] body)
        {
            try
            {
                _channel.BasicPublish(exchange: "", routingKey: _queueName, basicProperties: null, body: body);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task PrepareProductforSending(Events message)
        {
            try
            {
                var json = JsonConvert.SerializeObject(message);
                var body = Encoding.UTF8.GetBytes(json);

                await SendMessage(body);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
