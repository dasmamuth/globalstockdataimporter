﻿using System.Collections.Generic;

namespace GlobalStockDataImporter.Model
{
    public class SapDataModel
    {
        public string MATNR { get; set; }
        public string GRID { get; set; }
        public double QUANTITY { get; set; }
        public string TIMESTAMP { get; set; }
        public string KUNNR { get; set; }
        public string NAME1 { get; set; }
        public string COLOR { get; set; }
        public string CUP { get; set; }
        public string ZSIZE { get; set; }
        public string MATERIAL_PRICE_ZONE { get; set; }
    }

    public class SapDataModelwithETailorList
    {
        public string MATNR { get; set; }
        public string GRID { get; set; }
        public double QUANTITY { get; set; }
        public string TIMESTAMP { get; set; }
        public string KUNNR { get; set; }
        public string NAME1 { get; set; }
        public string COLOR { get; set; }
        public string CUP { get; set; }
        public string ZSIZE { get; set; }
        public string MATERIAL_PRICE_ZONE { get; set; }
        public List<string> ETailorList { get; set; }
    }
}
