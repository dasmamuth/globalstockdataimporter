﻿using System;
using System.Collections.Generic;

namespace GlobalStockDataImporter.Model
{
    public class JPNSQLDataModel
    {
        public class Entry1
        {
            public string Konzern { get; set; }
            public string Material { get; set; }
            public string COL { get; set; }
            public string CUP { get; set; }
            public string SIZE { get; set; }
            public string URL { get; set; }
            public string C_COL { get; set; }
            public string C_CUPSIZE { get; set; }
            public string reg_datetime { get; set; }
            public string mod_datetime { get; set; }
        }

        public class Root
        {
            public List<Entry1> Entry1 { get; set; }
        }
    }
}