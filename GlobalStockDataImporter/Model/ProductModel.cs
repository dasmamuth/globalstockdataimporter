﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace GlobalStockDataImporter.Model
{
    public class ProductModel
    {
        public class Rakuten
        {
            [JsonProperty("item001")]
            public List<string> ItemID { get; set; }
            [JsonProperty("item002")]
            public string Color { get; set; }
            [JsonProperty("item003")]
            public string Cup { get; set; }
        }

        public class Yahoo
        {
            [JsonProperty("item001")]
            public List<string> ItemID { get; set; }
            [JsonProperty("item002")]
            public string Color { get; set; }
            [JsonProperty("item003")]
            public string Cup { get; set; }
        }

        public class Jpn
        {
            public Rakuten Rakuten { get; set; }
            public Yahoo Yahoo { get; set; }
        }

        public class ETailorspec
        {
            public Jpn jpn { get; set; }
        }

        public class Sku
        {
            public string stockkeepingunit { get; set; }
            public string material { get; set; }
            public string color { get; set; }
            public string cup { get; set; }
            public string size { get; set; }
            [JsonProperty("etailorspec")]
            public ETailorspec etailorspec { get; set; }
        }

        public class Product
        {
            //public Guid Id { get; set; }
            public Sku sku { get; set; }
            public int stocklevel { get; set; }
            public string unit { get; set; }
            public DateTime timestamp { get; set; } = DateTime.Now;
            public List<string> channel { get; set; }
        }
    }
}
