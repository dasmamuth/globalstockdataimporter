﻿using System;
namespace GlobalStockDataImporter.Model
{
    public class Settings
    {
        public string ConnectionString;
        public string Database;
    }

    public class RabbitMqConfiguration
    {
            public string Hostname { get; set; }
            public string SAPInQueue { get; set; }
            public string SQLInQueue { get; set; }
            public string EventHandler { get; set; }
            public string OutProductQueue { get; set; }
            public string OutProductQueueSAPSpecial { get; set; }
            public string UserName { get; set; }
            public string Password { get; set; }
    }
    
}
