﻿using GlobalStockDataImporter.Interfaces;
namespace GlobalStockDataImporter.Model
{
    public class StockDatabaseSettings:IETailorsDatabaseSettings
    { 
        public string OrdersCollectionName { get; set; }
        public string ConnectionString { get; set; }
        public string DatabaseName { get; set; }
    }
}
