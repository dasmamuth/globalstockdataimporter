﻿using System;
using System.Collections.Generic;
using MongoDB.Entities;

namespace GlobalStockDataImporter.Model
{
    public class ETailors: Entity
    {
        public List<string> Number { get; set; }
        public string SapName { get; set; }
        public string SalesChannelName { get; set; }
    }
}
