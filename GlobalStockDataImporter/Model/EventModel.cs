﻿using System;
namespace GlobalStockDataImporter.Model
{
    public class EventModel
    {
       public class Events
       {
           public DateTime Date { get; set; }
           public string Service { get; set; }
           public string Process { get; set; }
           public string Event { get; set; }
       }
        
    }
}
