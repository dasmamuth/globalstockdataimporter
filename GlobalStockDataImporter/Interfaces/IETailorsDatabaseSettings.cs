﻿using System;
namespace GlobalStockDataImporter.Interfaces
{
    public interface IETailorsDatabaseSettings
    {
        string OrdersCollectionName { get; set; }
        string ConnectionString { get; set; }
        string DatabaseName { get; set; }
    }
}
