﻿using System;
using System.Threading.Tasks;
using static GlobalStockDataImporter.Model.EventModel;

namespace GlobalStockDataImporter.Interfaces
{
    public interface IEventHandlerSender
    {
        Task PrepareProductforSending(Events message);

        Task SendMessage(byte[] body);
    }
}
