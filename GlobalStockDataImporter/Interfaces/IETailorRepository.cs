﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using GlobalStockDataImporter.Model;

namespace GlobalStockDataImporter.Interfaces
{
    public interface IETailorRepository
    {
        Task<ETailors> GetEtailorName(string ETailorNumber);

        Task<List<ETailors>> GetAvailableETailors();
    }
}

