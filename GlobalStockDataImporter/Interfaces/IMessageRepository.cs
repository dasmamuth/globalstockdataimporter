﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using GlobalStockDataImporter.Model;

namespace GlobalStockDataImporter.Interfaces
{
    public interface IMessageRepository
    {
        Task<bool> ProcessSQLProductInformations(List<JPNSQLDataModel.Entry1> product);

        Task<bool> ProcessSAPProductInformations(List<SapDataModel> product);
    }
}
