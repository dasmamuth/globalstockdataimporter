﻿using System.Collections.Generic;
using System.Threading.Tasks;
using GlobalStockDataImporter.Model;

namespace GlobalStockDataImporter.Interfaces
{
    public interface IProductImportSender
    {
        //Sending the list of Products to Import in a plain way
        Task PrepareProductforSending(List<ProductModel.Product> product);

        //Sending the Products in a Dict with the Incoming Imoport information for SQL or SAP
        Task PrepareProductforSendingDict(Dictionary<string,List<ProductModel.Product>> product);

        //A prepared list containing the SKUs of products to import, used in the next step to validate against not changed products
        Task PrepareSAPCheckforSendingDict(Dictionary<string, List<string>> product);

        Task SendMessage(byte[] body);
    }
}
