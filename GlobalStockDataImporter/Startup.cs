using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GlobalStockDataImporter.Data;
using GlobalStockDataImporter.Interfaces;
using GlobalStockDataImporter.Messaging.Receiver.v1;
using GlobalStockDataImporter.Messaging.Sender.v1;
using GlobalStockDataImporter.Model;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using static GlobalStockDataImporter.Model.Settings;

namespace GlobalStockDataImporter
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.Configure<RabbitMqConfiguration>(Configuration.GetSection("RabbitMq"));

            services.AddControllers();

            services.AddSwaggerGen(options =>
            {
                options.CustomSchemaIds(type => type.ToString());
            });
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Microsoft.OpenApi.Models.OpenApiInfo
                {
                    Title = "GlobalStockDataImporter",
                    Version = "v2",
                    Description = "API to process Import Data from SAP and the JPN SQL Database for Rakuten and Yahoo",
                });
            });

            services.AddSingleton<MessagingRepository>();
            services.AddTransient<IMessageRepository, MessagingRepository>();

            services.AddHostedService<SapProductImportReceiver>();
            services.AddHostedService<JPNSQLProductImportReceiver>();

            services.AddTransient<IProductImportSender, ProductImportSender>();
            services.AddTransient<IEventHandlerSender, EventHandlerSender>();

            services.AddSingleton<ETailorRepository>();
            services.AddTransient<IETailorRepository, ETailorRepository>();

            services.Configure<StockDatabaseSettings>(Configuration.GetSection("MongoConnection"));
            services.AddSingleton<IETailorsDatabaseSettings>(sp => sp.GetRequiredService<IOptions<StockDatabaseSettings>>().Value);

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Global Stock Data SAP and JPN SQL Importer V2");
            });
        }
    }
}
